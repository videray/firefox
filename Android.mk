
LOCAL_PATH := $(call my-dir)


# https://download.mozilla.org/?product=fennec-latest&os=android&lang=multi
#
#$(LOCAL_PATH)/firefox.apk:
#	curl -o firefox.apk "https://download-installer.cdn.mozilla.net/pub/mobile/releases/68.1/android-api-16/multi/fennec-68.1.multi.android-arm.apk"

include $(CLEAR_VARS)
LOCAL_MODULE := Firefox
LOCAL_MODULE_CLASS := APPS
LOCAL_MULTILIB := 32
LOCAL_SRC_FILES := firefox.apk
LOCAL_CERTIFICATE := platform
LOCAL_MODULE_TARGET_ARCH := arm

LOCAL_PREBUILT_JNI_LIBS_arm := @lib/armeabi-v7a/libfreebl3.so \
								@lib/armeabi-v7a/liblgpllibs.so \
								@lib/armeabi-v7a/libmozavcodec.so \
								@lib/armeabi-v7a/libmozavutil.so \
								@lib/armeabi-v7a/libmozglue.so \
								@lib/armeabi-v7a/libnss3.so \
								@lib/armeabi-v7a/libnssckbi.so \
								@lib/armeabi-v7a/libplugin-container.so \
								@lib/armeabi-v7a/libsoftokn3.so \
								@lib/armeabi-v7a/libxul.so

include $(BUILD_PREBUILT)

